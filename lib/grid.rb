require 'ostruct'

class NoBlockException < Exception; end

# Grid provides a container which tracks a number of loci and provides access
# to them through its #at(x, y) method. It abstracts the navigation of the 
# grid so that it's very simple to retrieve a single square.
#
# In the future, the grid might provide a method of retrieving n random 
# squares. It might also provide a way of iterating over its collection so that
# one could simply print it.
class Grid
  attr_reader :width, :height, :area, :squares
  def initialize(width, height, &block)
    raise NoBlockException unless block_given?

    @width = width
    @height = height
    @area = width * height

    build_squares!(&block)
  end

  def build_squares!(&block)
    @squares = Array.new(@height) do |y|
      Array.new(@width) do |x|
        block.call(x, y)
      end
    end
  end

  def at(x, y)
    @squares[y][x]
  end
end
