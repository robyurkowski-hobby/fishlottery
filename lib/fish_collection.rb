class FishCollection
  include Enumerable

  def initialize
    @fish ||= []
  end

  def each
    @fish.each { |fish| yield fish }
  end

  def add(item)
    @fish << item
  end

end
