class Square
  attr_reader :x, :y, :pair
  attr_accessor :occupant
  def initialize(x, y)
    @x = x
    @y = y
    @pair = [@x, @y]
  end
end
