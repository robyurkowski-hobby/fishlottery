#require_relative 'fish_collection'

class Player
  attr_accessor :name
  attr_reader :x, :y, :square, :pair
  def initialize(name)
    @name = name
    @fish ||= []
  end

  def fish
    @fish
  end

  def catch!(item)
    @fish << item
  end

  def square=(square)
    @square = square
    @x = square.x
    @y = square.y
  end

  def pair
    if @x.nil? && @y.nil? then nil else [@x, @y] end
  end

end
