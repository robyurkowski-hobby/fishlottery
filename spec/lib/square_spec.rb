require 'ostruct'
require 'spec_helper'
require_relative '../../lib/square'

describe Square do
  let(:square) { Square.new 1, 4 }
  subject { square } 
  its(:x) { should == 1 }
  its(:y) { should == 4 }
  its(:pair) { should == [1, 4] }

  it "can have an occupant" do
    player = OpenStruct.new
    square.occupant = player
    square.occupant.should == player
  end
end

