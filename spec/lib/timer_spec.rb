require 'spec_helper'
require_relative '../../lib/timer'

describe Timer do
  it "demands a block" do
  end
  
  it "can be set to end after a certain amount of time" do
  end
  
  it "can be started at any time" do
  end
  
  it "operates in its own thread" do
  end
  
  it "calls a pre-specified block when it ends" do
  end
  
end