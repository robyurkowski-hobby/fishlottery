require 'ostruct'
require 'spec_helper'
require_relative '../../lib/game'

# The Game object should represent a single session of fishlottery.

describe Game do
  let(:game) { Game.new }
  let(:player_list) { [OpenStruct.new] }
  
  describe "player list" do
  
    it "maintains a list of players" do
      game.players.should be_a_kind_of(Array)
      game.players.count.should == 0
    end
    
    it "can accept new players before the game begins" do
      game.addPlayers player_list
      game.players.count.should == 1
    end
  end
  
  it "maintains a grid (which acts as the game board)"
    game.grid.should_not be_nil
  end
  
  let(:locus) { game.grid.flatten.first }
  
  it "starts a fish-biting timer when the game begins" do
    game.begin
    game.timer.should_not be_nil
  end
  
  # TODO: check that the game passes an end-of-game block to the timer
  
  context "when the timer ends" do
  # Is this an OK use of the context method?
  
    it "chooses the winning square randomly" do
      winner = game.pick_winning_square
      winner.should be_a_kind_of(locus.type)
      
      winner2 = game.pick_winning_square
      winner2.pair.should_not == winner1
    end
    
    it "awards fish to players on a winning square" do
      # TODO
    end
  end
  
end