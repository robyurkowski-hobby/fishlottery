require 'spec_helper'
require_relative '../../lib/fish_collection'

describe FishCollection do
  let(:collection) { FishCollection.new }

  it "is empty by default" do
    collection.count.should == 0
  end

  it "can be added to" do
    collection.add []
    collection.count.should == 1
  end

  specify "when adding persists the item" do
    collection.add []
    collection.should include([])
  end
end
