require 'ostruct'
require 'spec_helper'
require_relative '../../lib/player'

class Fish; end;

describe Player do
  let(:player) { Player.new "Bob" }
  let(:fish) { Fish.new }

  it "has a name" do
    player.name.should == "Bob"
  end

  it "can change its name" do
    player.name = "Alice"
    player.name.should == "Alice"
  end

  it "has a fish collection" do
    player.fish.should_not be_nil
  end

  it "can catch fish" do
    player.catch!(fish)
    player.fish.should include(fish)
  end

  it "knows how many fish it's caught" do
    player.catch!(fish)
    player.fish.count.should == 1
  end

  context "when square is set" do
    let(:square) { OpenStruct.new(:x => 1, :y => 4) }
    before(:each) do
      player.square = square
    end

    it "can be retrieved" do
      player.square.should == square
    end

    it "sets x value when assigned" do
      player.x.should == 1
    end

    it "sets y value when assigned" do
      player.y.should == 4
    end

    it "gives the player a location pair" do
      player.pair == [1, 4]
    end
  end

  context "when square is not set" do
    it "has no square" do
      player.square.should be_nil
    end

    it "has no x" do
      player.x.should be_nil
    end

    it "has no y" do
      player.y.should be_nil
    end

    it "has no pair" do
      player.pair.should be_nil
    end
  end
end
