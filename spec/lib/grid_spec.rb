require 'spec_helper'
require_relative '../../lib/grid'
require 'ostruct'

describe Grid do
  let(:grid) { Grid.new(10, 5) {|x, y| OpenStruct.new(:x => x, :y => y, :pair => [x, y]) } }

  it "demands a block" do
    lambda { newgrid = Grid.new(5, 5) }.should raise_error(NoBlockException)
  end

  it "knows its width" do
    grid.width.should == 10
  end

  it "knows its height" do
    grid.height.should == 5
  end

  it "knows its total number of squares" do
    grid.area.should == 50
  end

  describe "squares" do

    it "first index by y value" do
      grid.squares.count.should == grid.height
    end

    it "indexes second by x value" do
      grid.squares.first.count.should == grid.width
    end

    it "keeps reference of each locus" do
      grid.squares.flatten.should have(50).things
    end

    it "accepts a block to decide what represents a locus" do
      grid = Grid.new(5, 5) { 1 }
      grid.squares.flatten.first.should == 1
    end

    it "allows access to a particular locus" do
      grid.at(1, 4).pair.should == [1, 4]
    end
  end
end
